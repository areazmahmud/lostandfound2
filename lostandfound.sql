-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 02, 2017 at 06:42 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lostandfound`
--

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `item_category` varchar(255) NOT NULL,
  `item_description` text NOT NULL,
  `item_pic` varchar(255) NOT NULL,
  `item_key` varchar(11) NOT NULL,
  `item_create_dt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `item_update_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `user_id`, `item_name`, `item_category`, `item_description`, `item_pic`, `item_key`, `item_create_dt`, `item_update_dt`) VALUES
(22, 2, 'Watch', 'Personal', '&lt;p&gt;\r\n	&lt;strong&gt;Lorem Ipsum&lt;/strong&gt; is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&amp;#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it&lt;/p&gt;\r\n', 'wtch.jpg', 'JTGcDzq', '2017-03-01 18:00:00', '2017-03-02 00:00:00'),
(23, 51, 'Walet', 'Electric', '&lt;p&gt;\r\n	&lt;strong&gt;Lorem Ipsum&lt;/strong&gt; is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&amp;#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.&lt;/p&gt;\r\n', 'walet.jpg', 'j9gMgGE', '2017-03-01 18:00:00', '2017-03-02 00:00:00'),
(24, 1, 'Mobile', 'Personal', '&lt;p&gt;\r\n	&lt;strong&gt;Lorem Ipsum&lt;/strong&gt; is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&amp;#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.&lt;/p&gt;\r\n', 'mobile.jpg', 'wNbmhig', '2017-03-01 18:00:00', '2017-03-02 00:00:00'),
(25, 52, 'Tv', 'Electric', '&lt;p&gt;\r\n	&lt;strong&gt;Lorem Ipsum&lt;/strong&gt; is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&amp;#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.&lt;/p&gt;\r\n', 'tv.jpg', 'XqDgHzc', '2017-03-01 18:00:00', '2017-03-02 00:00:00'),
(26, 53, 'Bycicle', 'Personal', '&lt;p&gt;\r\n	&lt;strong&gt;Lorem Ipsum&lt;/strong&gt; is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&amp;#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.&lt;/p&gt;\r\n', 'cycle.jpg', 'tMDrlhJ', '2017-03-01 18:00:00', '2017-03-02 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `user_profile`
--

CREATE TABLE `user_profile` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_pass` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_number` varchar(11) NOT NULL,
  `user_address` varchar(255) NOT NULL,
  `user_country` varchar(255) NOT NULL,
  `user_dob` date NOT NULL,
  `user_des` text NOT NULL,
  `user_img` varchar(255) NOT NULL,
  `user_code` varchar(120) NOT NULL,
  `user_status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_profile`
--

INSERT INTO `user_profile` (`id`, `user_name`, `user_pass`, `user_email`, `user_number`, `user_address`, `user_country`, `user_dob`, `user_des`, `user_img`, `user_code`, `user_status`) VALUES
(1, 'Md. Shoriful Islam Arman', '123456', 'mdarman780@gmail.com', '452157878', '326/Gulabgh Malibagh, Dhaka', 'Dhaka', '0000-00-00', '&lt;p&gt;\r\n	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&amp;#39;s standard dummy text ever since the 1500s,&lt;/p&gt;\r\n', '11.jpg', 'WoXmldEZeP', 1),
(2, 'Murad', '123456', 'murad@gmail.com', '01959180034', 'xyz@gmail.com', 'Borishal', '2017-02-20', 'when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets', '2.jpg', 'WoXmldEZeP', 1),
(51, 'Mahmud', '123456', 'mahmud@gmail.com', '01959180030', '125/gulbagh Malibagh , Dhaka', 'Dhaka', '2017-02-22', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to ', '3.jpg', 'WoXmldEZeP', 1),
(52, 'Meezan', '123456', '123456@gmail.com', '234567890', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'Lorem Ipsum', '0000-00-00', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', '4.jpg', 'WoXmldEZeP', 1),
(53, 'Tarun', '123456', 'hello@gmail.com', '01959180030', '326/Gulabgh Malibagh, Dhaka', 'Khulna', '2017-02-22', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it ', '5.jpg', 'yKmAZKnBYJ', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_profile`
--
ALTER TABLE `user_profile`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `user_profile`
--
ALTER TABLE `user_profile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
